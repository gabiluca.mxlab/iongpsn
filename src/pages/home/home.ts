import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { BackgroundMode } from '@ionic-native/background-mode';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { Geolocation } from '@ionic-native/geolocation';

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {
	myCoords: { lat: number; lng: number };

	getMyGPSPosition(cb) {
		this.geolocation
			.getCurrentPosition({ enableHighAccuracy: true })
			.then((position) => {
				// if (!position.coords.latitude) {
				// 	return;
				// }
				this.localNotifications.schedule({
					launch: true,
					text: 'ppp\n'
				});
				if (cb) cb({ lat: position.coords.latitude, lng: position.coords.longitude });
			})
			.catch((error) => {
				cb(error);
				console.log('error', error);
			});
	}

	onGPSPosition(p) {
		try {
			this.myCoords.lat = p.coords.latitude;
			this.myCoords.lng = p.coords.longitude;
		} catch (e) {}
		this.localNotifications.schedule({
			launch: true,
			text: 'Notificare din bmode...\n' + JSON.stringify(p)
		});
		this.onBkModeActive();
	}

	onBkModeActive() {
		setTimeout(() => {
			this.getMyGPSPosition((p) => this.onGPSPosition(p));
		}, 15 * 1000);
	}

	constructor(
		public localNotifications: LocalNotifications,
		public backgroundMode: BackgroundMode,
		public platform: Platform,
		public navCtrl: NavController,
		public geolocation: Geolocation
	) {
		platform.ready().then(() => {
			this.backgroundMode.on('activate').subscribe(() => this.onBkModeActive());
			this.backgroundMode.enable();

			// this.getMyGPSPosition((p) => alert(JSON.stringify(p)));
		});
	}
}
